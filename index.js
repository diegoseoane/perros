class Perro {
    constructor(nombre, raza, estado){
        this.nombre = nombre;
        this.raza = raza;
        this.estado = estado;
    }

    modificar(nuevoEstado){
        this.estado = nuevoEstado;  
    }

    informar(){
        console.log("El estado de ", this.nombre, " es: ", this.estado);
    }
}

perro1 = new Perro ("Pipo", "Siberiano", "En proceso de adopción");
perro2 = new Perro ("Negror", "Manto Negro", "Adoptado");
perro3 = new Perro ("Jack", "Siberiano", "En adopción");
perro4 = new Perro ("Felipe", "Labrador", "Adoptado");

listaPerros = [];
listaPerros.push(perro1, perro2, perro3, perro4);

function load() {
    let nombrePerro = prompt("Ingrese nombre de perro: ");
    let razaPerro = prompt("Ingrese raza de perro: ");
    let estadoPerro = prompt("Ingrese el estado de adopción: ");
    
    if (nombrePerro === "" || razaPerro === ""){
        alert("Los campos no pueden estar vacíos");
        load();
    } else if (estadoPerro !== "Adoptado" && estadoPerro !== "En adopción" && estadoPerro !== "En proceso de adopción"){
        alert("El estado no es válido");
        load();
    } else {
        listaPerros.unshift(new Perro(nombrePerro, razaPerro, estadoPerro));
        ask();
    }
}

function ask() {
    if (window.confirm("¿Desea cargar otro perro?")){
        load();
    } else {
        show();
    }
}

function show() {
    
    console.log("Lista de perros: ");
    for (let perro of listaPerros){
        console.log(perro.nombre);
    }
    console.log("Perros adoptados: ");
    for (let perro of listaPerros){
        if (perro.estado === "Adoptado") {
            console.log(perro.nombre);
        }
    }

    console.log("Perros en proceso de adopción: ");        
    for (let perro of listaPerros){
        if (perro.estado === "En proceso de adopción") {
            console.log(perro.nombre);
        }
    }

    console.log("Perros en adopción: ");        
    for (let perro of listaPerros){
        if (perro.estado === "En adopción") {
            console.log(perro.nombre);
        }
    }
}

load();